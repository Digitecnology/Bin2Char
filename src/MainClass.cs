// created on 25/05/2006 at 02:58 a
using System;

public class MainClass
{
	public MainClass(string[] Binaries)
	{
		bool[] temp;
		
		Console.WriteLine("--------------VALUE--------------");
				
		for(int i = 0; i <= Binaries.Length - 1; i++)
		{		
			temp = new bool[8];
			if(Binaries[i].Length == 8)
			{					
				for(int a = 0; a <= temp.Length - 1; a++)
				{					
					temp[a] = Convert.ToBoolean(Convert.ToInt32(Binaries[i].Substring(a,1)));												
				}
			}			
			Console.Write(Convert.ToString(Convert.ToChar(this.BinaryToByte(temp))));		
		}	
		Console.WriteLine();
		Console.WriteLine("---------------------------------");		
	}
	
	public byte BinaryToByte(bool[] BinaryNumber)
	{
		int temp = 0;
		int[] values = new int[]{128,64,32,16,8,4,2,1};
		
		if(BinaryNumber.Length == 8)
		{
			for(int i = 0;i <= BinaryNumber.Length - 1; i++)
			{
				if(BinaryNumber[i] == true)
				{
					temp += values[i];					
				}
			}
		}
		
		return Convert.ToByte(temp);
	}
	
	static void Main(string[] args)
	{
		new MainClass(args);
	}
}